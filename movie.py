'''
PROJETO: mini IMDB local
FUNCIONALIDADE:
- documentar filmes assistidos pelo usuário
 * nome
 * nota (0 a 10)
 * descrição
 * qualquer outra coisa que você ache útil
- menuzinho no console pra conseguir navegar pelos filmes
- no meu, função pra adicionar filmes, remover filmes, e printar os filmes já adicionados
'''
import os, time

movies = {
    "A volta dos que não foram" : {
        "Score" : 9.9,
        "Description" : "Um lixo",
        "Author" : "Matheus Wagner",
        "Date" : "03,06,1998",
        "Tag" : "Aventura",
    },
    "Morte do Demonio" : {
        "Score" : 0.5,
        "Description" : "Pior do que a morte",
        "Author" : "Alister Crawley",
        "Date" : "06,06,1666",
        "Tag" : "Devil"
    }
}

class Control:
    def add_film(self):
        n = input("Qual o nome do filme visto? \n")
        s = input("Qual o score do ao filme? \n")
        d = input("Qual a descrição do filme? \n")
        a = input("Qual o autor do filme? \n")
        dt = input("Qual a data do filme? \n")
        t = input("Qual tag classifica o filme? \n")
        print()

        movies[n] = {}
        movies[n]["Score"] = s
        movies[n]["Description"] = d
        movies[n]["Author"] = a
        movies[n]["Date"] = dt
        movies[n]["Tag"] = t

    def rmv_film(self):
        self.list_film()
        r = input("Qual o nome do filme a ser apagado? \n")
        movies.pop(r)
        print(f"O filme de nome {r} foi deletado.")

    def list_film(self):
        for keys , values in movies.items():
            print(keys,values)

    def menu(self):
        while True:
            t = input("1 -> Adicionar Filme \n" +
            "2 -> Deletar Filme \n" +
            "3 -> Listar FIlmes \n" +
            "4 -> Sair \n")

            if t=="1":
                self.add_film()

            elif t=="2":
                self.rmv_film()

            elif t=="3":
                print("\n")
                self.list_film()
                time.sleep(1)
                print("\n")

            elif t=="4":
                print("Obrigado, até a próxima! \n" )
                break

            elif t !="":
                print("Digite alguma das alternativas!")
                time.sleep(1.5)
                os.system("clear")

Control().menu()