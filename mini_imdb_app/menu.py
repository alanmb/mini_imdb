from movies import Movie
from termcolor import cprint
from os import system

def get_user_option():
    print("Would you like to")
    print("1 - list your movies")
    print("2 - add a movie")
    print("3 - edit a movie")
    print("4 - remove a movie")
    print("5 - display movie information")
    print("q - quit application")
    option = input()
    return option

def clear():
    system("clear")    

def list_movies(movies):
    clear()
    for i, movie in enumerate(movies):
        cprint(f"{i}: {movie.name}", "blue", attrs=["bold"])

def remove_movie(movies):
    list_movies(movies)
    cprint("what movie would you like to remove? (insert id, example: 3)", "blue", attrs=["bold"])
    index_option = int(input(">"))
    movie_name = movies[index_option].name
    movies.pop(index_option)
    cprint(f"{movie_name} removed!", "green", attrs=["bold"])
    return movies

def add_movie(movies):
    movie = Movie(manual=True)
    movies.append(movie)
    cprint(f"Movie added!", "green", attrs=["bold"])
    return movies

def edit_movie(movies):
    cprint("what movie would you like to edit? (insert id, example: 3)", "blue", attrs=["bold"])
    list_movies(movies)
    index_option = int(input(">"))

    display_movie(movies, index_option)
    cprint("What would you like to edit", "blue", attrs=["bold"])
    cprint("1 - name", "blue", attrs=["bold"])
    cprint("2 - rating", "blue", attrs=["bold"])
    cprint("3 - description", "blue", attrs=["bold"])
    cprint("4 - genre", "blue", attrs=["bold"])
    user_option = input(">")

    if user_option == "1":
        movies[index_option].set_name()
    elif user_option == "2":
        movies[index_option].set_rating()
    elif user_option == "3":
        movies[index_option].set_description()
    elif user_option == "4":
        movies[index_option].set_genre()

    cprint(f"Movie edited!", "green", attrs=["bold"])
    cprint(f"{movies[index_option].name}\n{movies[index_option].rating}\n{movies[index_option].description}\n{movies[index_option].genre}", "yellow", attrs=["bold"])
    return movies

def display_movie(movies, i=None):
    clear()
    if i == None:
        cprint("what movie would you like to display? (insert id, example: 3)", "blue", attrs=["bold"])
        list_movies(movies)
        i = int(input(">"))
    cprint(f"{movies[i].name}\n{movies[i].rating}\n{movies[i].description}\n{movies[i].genre}", "yellow", attrs=["bold"])

def menu(movies):
    clear()
    while True:
        print()
        user_option = get_user_option()

        if user_option == "1":
            list_movies(movies)
        elif user_option == "2":
            add_movie(movies)
        elif user_option == "3":
            edit_movie(movies)
        elif user_option == "4":
            remove_movie(movies)
        elif user_option == "5":
            clear()
            list_movies(movies)
            index_option = int(input("Select movie: "))
            display_movie(movies, index_option)
        elif user_option == "q":
            quit()
        else:
            clear()
            cprint("VOCÊ É BURRO?", "red", attrs=["bold", "blink"])
