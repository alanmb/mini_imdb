from menu import menu
from movies import Movie

def main():
    f = open("./movies.csv", "r")
    r = f.read().split("\n")
    f.close()

    movies_from_files = [i.split(",") for i in r]
    movies_from_files.pop(0) # removes header
    # movies_from_files format: [[dsad, dsabdas, dsjaid, ewiq], [ewqd13, 32190312, ndsh72, 666], ...]
    movies = []

    for i in movies_from_files:
        movies.append(Movie(i[0], i[1], i[2], i[3]))

    menu(movies)

main()