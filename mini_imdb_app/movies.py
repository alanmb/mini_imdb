class Movie:
    def __init__(self, name=None, rating=None, description=None, genre=None, manual=False):
        if manual:
            self.manually()
        else:
            self.name = name
            self.rating = rating
            self.description = description
            self.genre = genre
    
    def manually(self):
        self.set_name()
        self.set_rating()
        self.set_description()
        self.set_genre()

    def set_name(self):
        self.name = input("Type the movie's name: ")
    
    def set_rating(self):
        self.rating = input("Type the movie's rating: ")
    
    def set_description(self):
        self.description = input("Type the movie's description: ")

    def set_genre(self):
        self.genre = input("Type the movie's genre: ")